package jsonrpc

import (
	"bufio"
	"context"
	"errors"
	"fmt"
	"net"
	"time"
)

var (
	errMultiReplyTypePtr = errors.New("multi reply should be arrry or slice pointer")
	errEmptyCodec        = errors.New("client has an empty codec")
	errNotSupportMulti   = errors.New("current codec not support multi request")
	errCtxTimeout        = errors.New("timeout")
)

type Client struct {
	// rpc server addr over tcp
	tcpAddr string

	// rpc server addr over http
	// httpAddr string

	// codec to manage about the request and response encoding and decoding
	codec ClientCodec

	// connection to the tcp server
	tcpConn net.Conn
}

func NewClientWithCodec(codec ClientCodec, tcpAddr string) *Client {
	if codec == nil {
		codec = NewJSONCodec()
	}

	return &Client{
		tcpAddr: tcpAddr,
		// httpAddr: httpAddr,
		codec: codec,
	}
}

func (c *Client) Call(method string, args, reply interface{}) error {
	Log.Debug("a new call")
	req := c.codec.NewRequest(method, args)
	resps := make([]jsonResponse, 0)
	if err := c.calltcp([]jsonRequest{req}, &resps); err != nil {
		Log.Debug("could not calltcp err=%v", err)
		return err
	}

	resp := resps[0]
	Log.Debug("len(resps)=%d, resp.Reply()=%s", len(resps), resp.Reply())
	if err := c.codec.ReadResponseBody(resp.Reply(), reply); err != nil {
		Log.Debug("could not ReadReponseBody err=%v", err)
		return err
	}
	Log.Debug("call done")
	return nil
}

// Call server over tcp
func (c *Client) calltcp(reqs []jsonRequest, resps *[]jsonResponse) (err error) {
	if err = c.valid(); err != nil {
		return err
	}

	var (
		wr    = bufio.NewWriter(c.tcpConn)
		rr    = bufio.NewReader(c.tcpConn)
		psend = ProtoNew()
		precv = ProtoNew()
	)

	timeoutCtx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	select {
	case <-timeoutCtx.Done():
		return errCtxTimeout
	default:
		// req := c.codec.NewRequest(method, args)
		if psend.Body, err = c.codec.EncodeRequests(&reqs); err != nil {
			Log.Debug("could not EncodeRequests, err=%v", err)
			return err
		}

		if err := psend.WriteTCP(wr); err != nil {
			Log.Debug("could not WriteTCP, err=%v", err)
			return err
		}
		wr.Flush()

		// recv from TCP response
		if err := precv.ReadTCP(rr); err != nil {
			Log.Debug("could not ReadTCP, err=%v", err)
			return err
		}

		Log.Debug("recv response body: %s", precv.Body)
		// var resp Response
		*resps, err = c.codec.ReadResponse(precv.Body)
		if err != nil {
			Log.Debug("could not ReadResponses, err=%v", err)
			return err
		}
	}

	return nil
}
func (c *Client) valid() error {
	if c.codec == nil {
		return errEmptyCodec
	}

	// connect to server
	if c.tcpConn == nil {
		conn, err := net.Dial("tcp", c.tcpAddr)
		if err != nil {
			return fmt.Errorf("net.Dial tcp get err: %v", err)
		}
		c.tcpConn = conn
	}
	return nil
}

// Close the client connectio to the server
func (c *Client) Close() {
	if c.tcpConn == nil {
		return
	}
	if err := c.tcpConn.Close(); err != nil {
		Log.Debug("could not close c.tcpConn, err=%v", err)
	}
}
