# Go实现 jsonrpc 

jsonrpc 服务端实现

```go
//服务端使用
// Int .
type Int struct{}

// Args ... for Sum Method
type Args struct {
	A int `json:"a"`
	B int `json:"b"`
	C int `json:"c"`
}

// Result .
type Result struct {
	Sum int `json:"sum"`
}

// Add ...
func (i *Int) Add(args *Args, reply *Result) error {
	reply.Sum = args.A + args.B
	return nil
}

// Sum ...
func (i *Int) Sum(args *Args, reply *Result) error {
	reply.Sum = args.A + args.B
	return nil
}
func main() {
	srv := jrpc.NewServerWithCodec(jrpc.NewJSONCodec())
	srv.Register(new(Int))
	http.Handle("/", srv)
	http.ListenAndServe(":8001", nil)
}
```

客户端调用Json格式

```json
//http
{
     "jsonrpc": "2.0",
     "method": "Int.Add",
     "params": {
        "a":21,
		"b":11
     },
     "id": "1"
}
```



主要代码来源：https://github.com/yeqown/rpc



#### 修改后使用方法

```go
import (
	"net/http"
	jrpc "gitee.com/gouyabin/jsonrpc"
)

func InitSer(){
    srv := jrpc.NewServerWithCodec(jrpc.NewJSONCodec())
	srv.AuthCheck = new(jrpc.Auth)
	srv.AuthCheck.AuthState = true  //启用Auth认证
	srv.AuthCheck.NoAuthMethod = []string{"User.Login"} //不需要auth认证的接口
	srv.AuthCheck.AuthCheckFunc = AuthCheck //AuthCheck认证方法
    srv.Register(&Int{}) //接上面例子
    http.Handle("/api", srv)
	http.ListenAndServe(":8080", nil)
    
}

//认证方法
func AuthCheck(a string) bool {
    if a=="1"{
        return true
    }
    return false // a(auth)校验失败
}
```

认证的接口格式

```json
//没有认证
{
    "jsonrpc": "2.0",
    "method": "user.login",
    "params": {
        "user": "Admin",
        "password": "admin"
    },
    "id": 1,
    "auth": null
}

//有认证
{
    "jsonrpc": "2.0",
    "method": "user.login",
    "params": {
        "user": "Admin",
        "password": "admin"
    },
    "id": 1,
    "auth": "0424bd59b807674191e7d77572075f33"
}
```

